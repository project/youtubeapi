2017-05-15  : 1.2.0
-------------------
- Adapt drupal coding standard
- Cleanup
- Add read me
- Move builders to youtubeapi_build


2016-12-14  : 1.1.1
-------------------
- Rename type (string boolean ....) to apitype
