<?php

namespace Drupal\youtubeapi;

/**
 * YoutubeAPIService.
 */
class YoutubeAPIService {

  /**
   * Get Configuration Name.
   */
  public static function getConfigName() {
    return 'youtubeapi.settings';
  }

}
