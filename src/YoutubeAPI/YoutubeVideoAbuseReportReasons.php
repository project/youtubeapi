<?php

namespace Drupal\youtubeapi\YoutubeAPI;

/**
 * YoutubeVideoAbuseReportReasons.
 * Youtube API Class.
 * Generated on : 2017-05-15 12:15:19.
 */
class YoutubeVideoAbuseReportReasons extends API {

  // API URL.
  const request_uri = "https://www.googleapis.com/youtube/v3/videoAbuseReportReasons";
  // API URL Part.
  const method = "videoAbuseReportReasons";

  // Request Parameters.
  const part = 'part';
  const hl = 'hl';

  // Response Parameters.
  const kind = 'kind';
  const etag = 'etag';
  const items = 'items';

}
