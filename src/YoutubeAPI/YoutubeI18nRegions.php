<?php

namespace Drupal\youtubeapi\YoutubeAPI;

/**
 * YoutubeI18nRegions.
 * Youtube API Class.
 * Generated on : 2017-05-15 12:15:17.
 */
class YoutubeI18nRegions extends API {

  // API URL.
  const request_uri = "https://www.googleapis.com/youtube/v3/i18nRegions";
  // API URL Part.
  const method = "i18nRegions";

  // Request Parameters.
  const part = 'part';
  const hl = 'hl';

  // Response Parameters.
  const kind = 'kind';
  const etag = 'etag';
  const items = 'items';

}
