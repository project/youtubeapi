<?php

namespace Drupal\youtubeapi\YoutubeAPI;

/**
 *
 */
interface APIInterface {

  /**
   * Returns API method.
   *
   * @return string
   *   API method.
   */
  public function getUrl();

  /**
   * Returns API Url.
   *
   * @return string
   *   API Url.
   */
  public static function getMethod();

  /**
   * Returns all available Parameters.
   *
   * @return array
   *   All available Parameters.
   *
   * @throws \Masterminds\HTML5\Exception
   */
  public function getParameters();

  /**
   * Returns all Mandatory Parameters.
   *
   * @return array
   *   Mandatory Parameters.
   */
  public function getMandatoryParameters();

}
