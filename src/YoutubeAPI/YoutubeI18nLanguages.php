<?php

namespace Drupal\youtubeapi\YoutubeAPI;

/**
 * YoutubeI18nLanguages.
 * Youtube API Class.
 * Generated on : 2017-05-15 12:15:17.
 */
class YoutubeI18nLanguages extends API {

  // API URL.
  const request_uri = "https://www.googleapis.com/youtube/v3/i18nLanguages";
  // API URL Part.
  const method = "i18nLanguages";

  // Request Parameters.
  const part = 'part';
  const hl = 'hl';

  // Response Parameters.
  const kind = 'kind';
  const etag = 'etag';
  const items = 'items';

}
